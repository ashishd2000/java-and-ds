class myT extends Thread {
	myT(ThreadGroup tg,String str) {
		super(tg,str) ;
	}
	public void run() {
		System.out.println(Thread.currentThread()) ;
	}
}
class ThreadGroupDemo {
	public static void main(String[] args) {
		ThreadGroup pthreadGP=new ThreadGroup("C2W") ;

		myT obj1=new myT(pthreadGP,"c") ;
		myT obj2=new myT(pthreadGP,"java") ;
		myT obj3=new myT(pthreadGP,"python") ;
		obj1.start() ;
		obj2.start() ;
		obj3.start() ;

		ThreadGroup cThreadG=new ThreadGroup(pthreadGP,"childG") ;
		
		myT obj4=new myT(cThreadG,"Flutter") ;
		myT obj5=new myT(cThreadG,"ReactJs") ;
		myT obj6=new myT(cThreadG,"SpringBoot") ;

		obj4.start() ;
		obj5.start() ;
		obj6.start() ;
	}
}

