class myT extends Thread {
	public void run() {
		System.out.println(Thread.currentThread().getName()) ;
		for (int i=0 ; i<10 ; i++) {
			System.out.println("In run") ;

			try {
				Thread.sleep(3000) ;  /*sleep throw InterruptedException */
			}catch(InterruptedException obj) {

			}
		}
	}
}
class ThreadDemo {
	public static void main(String[] args)throws InterruptedException {
		System.out.println(Thread.currentThread().getName()) ;
		myT obj=new myT() ;  //new born thread
		obj.start() ;  //automatic call to run method		
		
		for (int i=0 ; i<10 ; i++) {
			System.out.println("In main") ;
			Thread.sleep(3000) ;
		}
	}
}
