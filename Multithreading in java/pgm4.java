class demo extends Thread {
	public void run() {
		System.out.println("demo:"+Thread.currentThread().getName()) ;
	}
}
class myT extends Thread {
	public void run() {
		System.out.println("myT:"+Thread.currentThread().getName()) ;

		demo obj=new demo() ;
		obj.start() ;
	}
}
class ThreadDemo {
	public static void main(String[] args) {
		System.out.println("ThreadDemo:"+Thread.currentThread().getName()) ;
		
		myT obj=new myT() ;
		obj.start() ;
	}
}
