class myT extends Thread {
	public void run() {
		System.out.println(Thread.currentThread().getName()) ;
	}
}
class ThreadYieldDemo {
	public static void main(String[] args) {
		myT obj=new myT() ;
		obj.start() ;
		obj.yield() ;
		System.out.println(Thread.currentThread().getName()) ;
	}
}
