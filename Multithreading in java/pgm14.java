class myT extends Thread {
	myT(ThreadGroup tg,String str) {
		super(tg,str) ;
	}
	public void run() {
		System.out.println(Thread.currentThread()) ;
	}
}
class ThreadGroupDemo {
	public static void main(String[] args) {
		ThreadGroup pthreadGP=new ThreadGroup("C2W") ;

		myT obj1=new myT(pthreadGP,"c") ;
		myT obj2=new myT(pthreadGP,"java") ;
		myT obj3=new myT(pthreadGP,"python") ;
		obj1.start() ;
		obj2.start() ;
		obj3.start() ;
	}
}

