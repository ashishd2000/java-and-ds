//Creating child thread using runnable interface

class myT implements Runnable {
	public void run() {
		System.out.println("In run") ;
		System.out.println(Thread.currentThread().getName()) ;
	}
}
class ThreadDemo {
	public static void main(String[] args) {
		myT obj=new myT() ;
		Thread t=new Thread(obj) ;
		t.start() ;
		System.out.println(Thread.currentThread().getName()) ;
	}
}
