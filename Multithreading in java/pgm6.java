//Priority of thread  (min-0  normal-5  max-10)

class myT extends Thread {
	public void run() {
		Thread t=Thread.currentThread() ;
		System.out.println(t.getPriority()) ;
	}
}
class ThreadDemo {
	public static void main(String[] args) {
		Thread t=Thread.currentThread() ;
		System.out.println(t.getPriority()) ;

		myT obj1=new myT() ;
		obj1.start() ;

		t.setPriority(7) ;

		myT obj2=new myT() ;
		obj2.start() ;
	}
}
