class myT extends Thread {
	public void run() {
		System.out.println("In run") ;
		System.out.println(Thread.currentThread().getName()) ;
	}
	public void start() {
		System.out.println("In myT start") ;
		run() ;
	}
}
class ThreadDemo {
	public static void main(String[] args) {
		myT obj=new myT() ;
		obj.start() ;
		System.out.println(Thread.currentThread().getName()) ;
	}
}
