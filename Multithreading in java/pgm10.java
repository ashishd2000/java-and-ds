class myT extends Thread {
	public void run() {
		for (int i=0 ; i<5 ; i++) {
			System.out.println("In thread-0") ;
		}
	}
}
class ThreadDemo {
	public static void main(String[] args)throws InterruptedException {
		myT obj=new myT() ;
		obj.start() ;
		obj.join() ;

		for (int i=0 ; i<5 ; i++) {
			System.out.println("In main") ;
		}
	}
}
