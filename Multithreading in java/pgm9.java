/* Concurrency methods in thread class 
 * sleep-interrupted exception,native method
 * join-interrupted exception,native method
 * yeild-native method  */

class myT extends Thread {
	public void run() {
		System.out.println(Thread.currentThread()) ;
	}
}
class ThreadDemo {
	public static void main(String[] args) throws InterruptedException {
		System.out.println(Thread.currentThread()) ;

		myT obj=new myT() ;
		obj.start() ;

		Thread.sleep(1000) ;
		Thread.currentThread().setName("C2W") ;
		System.out.println(Thread.currentThread()) ;
	}
}


