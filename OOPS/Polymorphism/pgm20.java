class parent {
	private void fun() {
		System.out.println("parent fun") ;
	}
}
class child extends parent {
	void fun() {
		System.out.println("child fun") ;
	}
}
class client {
	public static void main(String[] args) {
		parent obj=new child() ;  /* error-fun() has private access in parent */
		obj.fun() ;
	}
}

