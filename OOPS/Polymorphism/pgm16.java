class parent {
	char fun() {
		System.out.println("Parent fun") ;
		return 'a' ;
	}
}
class child extends parent {
	int fun() {
		System.out.println("Child fun") ;
		return 10 ;
	}
}
class client {
	public static void main(String[] args) {
		parent obj=new child() ;
		obj.fun() ;
	}
}
/*ERROR-*
 * fun() in child cannot override fun() in parent
        int fun() {
            ^
  return type int is not compatible with char       */
