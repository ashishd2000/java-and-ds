class parent {
	int x=10 ;
	void m1() {
		System.out.println("In parent m1") ;
	}
}
class child extends parent {
	int a=20 ;
	void m1() {
		System.out.println("In child m1") ;
	}
}
class demo {
	demo (parent p) {
		System.out.println("In constructor-parent") ;
		p.m1() ;
	}
	demo (child c) {
		System.out.println("In constructor-child") ;
		c.m1() ;
	}
	public static void main(String[] args) {
		demo obj1=new demo(new parent()) ;

		demo obj2=new demo(new child()) ;
	}
}
