class demo {
	int fun(int x) {        //fun(int)
		System.out.println(x) ;
		return x ;
	}
		
	float fun(int x) {      //fun(int)
		System.out.println(x) ;
		return x ;
	}
	
}
/* ERROR-method fun(int) is already defined in
 *       class demo   
 */       

