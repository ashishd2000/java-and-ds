class parent {
	void fun() {
		System.out.println("parent fun") ;
	}
}
class child extends parent {
	private void fun() {
		System.out.println("child fun") ;
	}
}
/* error
 * attempting to assign weaker access privileges; was package */
