class parent {
	parent() {
		System.out.println("In parent constructor") ;
	}
	
	void fun() {
		System.out.println("In fun") ;
	}
}
class child extends parent {
	child() {
		System.out.println("In child constructor") ;
	}

	void gun() {
		System.out.println("In gun") ;
	}
}
class client {
	public static void main(String[] args) {
		child obj1=new child() ;
		obj1.fun() ;
		obj1.gun() ;

		parent obj2=new child() ;
		obj2.fun() ;
/*		obj2.gun() ;       error-cannot find symbol  */

/*		child obj3=new parent() ;   error-Incompatible type
       		                            parent cannot be converted to child  */
	}
}


