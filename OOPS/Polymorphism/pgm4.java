class parent {
	parent() {
		System.out.println("In parent constructor") ;
	}
	
	void property() {
		System.out.println("Home,Car,Gold") ;
	}

	void marry() {
		System.out.println("Deepika Padukone") ;
	}
}
class child extends parent {
	child() {
		System.out.println("In child constructor") ;
	}

	void marry() {
		System.out.println("Alia Bhatt") ;
	}
}
class client {
	public static void main(String[] args) {
		child obj=new child() ;
		obj.property() ;
		obj.marry() ;
	}
}


