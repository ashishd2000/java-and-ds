class parent {
	final void fun() {
	       System.out.println("parent fun")	;
	}
}
class child extends parent {
	void fun() {
		System.out.println("Child fun") ;
	}
}
/*error -
 *  fun() in child cannot override fun() in parent
   overridden method is final       */

