class parent {
	public void fun() {
		System.out.println("parent fun") ;
	}
}
class child extends parent {
	void fun() {
		System.out.println("Child fun") ;
	}
}
/* error
 * attempting to assign weaker access privileges; was public  */
