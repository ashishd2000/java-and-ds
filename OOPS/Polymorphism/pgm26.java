class demo {
	demo() {
		System.out.println("In constructor") ;
	}
}
class demochild extends demo {
	demochild() {
		System.out.println("In constructor-demochild") ;
	}
}
class parent {
	parent() {
		System.out.println("In parent constructor") ;
	}
	demo m1() {
		System.out.println("In m1 parent") ;
		return new demo() ;
	}
}
class child extends parent {
	child() {
		System.out.println("In constructor child") ;
	}
	demochild m1() {
		System.out.println("In m1 child") ;
		return new demochild() ;
	}
}
class client {
	public static void main(String[] args) {
		parent p=new child() ;
		p.m1() ;
	}
}

