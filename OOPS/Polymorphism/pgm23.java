class parent {
	static void fun() {
		System.out.println("parent fun") ;
	}
}
class child extends parent {
	void fun() {
		System.out.println("child fun") ;
	}
}
/* error-
 * fun() in child cannot override fun() in parent
        void fun() {
             ^
  overridden method is static             */
