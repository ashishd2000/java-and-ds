class demo {
	void fun(int x) {  //fun(int)
		System.out.println(x) ;
	}
	
	void fun(float y) {  //fun(float)
		System.out.println(y) ;
	}
	
	void fun(demo obj) {  //fun(demo)
		System.out.println(obj) ;
	}

	public static void main(String[] args) {
		demo obj=new demo() ;
		
		obj.fun(10) ;
		obj.fun(10.5f) ;

		demo obj1=new demo() ;
		obj1.fun(obj) ;
	}
}
