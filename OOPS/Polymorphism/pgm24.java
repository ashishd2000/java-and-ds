class parent {
	static void fun() {
		System.out.println("parent fun") ;
	}
}
class child extends parent {
	static void fun() {
		System.out.println("child fun") ;
	}
}
class client {
	public static void main(String[] args) {
		parent obj1=new parent() ;
		obj1.fun() ;                  //parent fun
		
		child obj2=new child() ;
		obj2.fun() ;                  //child fun
		
		parent obj3=new child() ;
		obj3.fun() ;                  //parent fun
	}
}
