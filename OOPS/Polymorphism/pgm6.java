class parent {
	parent() {
		System.out.println("In parent constructor") ;
	}
	
	void fun() {
		System.out.println("In fun parent") ;
	}
}
class child extends parent {
	child() {
		System.out.println("In child constructor") ;
	}

	void fun() {
		System.out.println("In fun child") ;
	}
}
class client {
	public static void main(String[] args) {
		parent obj=new child() ;
		obj.fun() ;
	}
}
		


