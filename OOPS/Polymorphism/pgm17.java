//Covarient return type:-parent child relation in return type 

class parent {
	Object fun() {
		System.out.println("Parent fun") ;
		return new Object() ;
	}
}
class child extends parent {
	String fun() {
		System.out.println("Child fun") ;
		return "Shashi" ;
	}
}
class client {
	public static void main(String[] args) {
		parent obj=new child() ;
		obj.fun() ;
	}
}
       		       
