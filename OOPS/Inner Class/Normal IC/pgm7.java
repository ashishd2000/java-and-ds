class Outer {
	int x=10 ;
	static int y=20 ;

	class Inner {
		int a=30 ;
		static int b=40 ;  /* ERROR
				      illegal static declaration in 
				      inner class Outer.Inner  */
	}
}
