class Outer {
	class Inner {
		void fun2() {
			System.out.println("Inner") ;
		}
	}
	void fun1() {
		System.out.println("Outer") ;
	}
}
class client {
	public static void main(String[] args) {
		Outer obj=new Outer() ;

		Outer.Inner obj1=obj.new Inner() ;   /* Outer$Inner(obj1,obj)  */
		obj1.fun2() ;
		
	}
}

