class Outer {
	void m1() {
		System.out.println("In m1-outer") ;
	}
	static class Inner {   /* Outer$Inner.class */
		void m1() {
			System.out.println("In m1-inner") ;
		}
	}
}
class client {
	public static void main(String[] args) {
		Outer.Inner obj=new Outer.Inner() ;
		obj.m1() ;
	}
}

