class parent {
	parent() {
		System.out.println("In parent class") ;
	}

	void parentProperty() {
		System.out.println("Flat,car,gold") ;
	}
}
class child extends parent {
	child() {
		System.out.println("In child constructor") ;
	}
}
class client {
	public static void main(String[] args) {
		child obj=new child() ;
		obj.parentProperty() ;
	}
}
