class demo {
	int x=10 ;
	
	demo() {
		System.out.println("No-args constructor") ;
	}

	demo(int y) {
		System.out.println("Para constructor") ;
	}

	public static void main(String[] args) {
		demo obj1=new demo() ;
		demo obj2=new demo(10) ;
		
	}
}
