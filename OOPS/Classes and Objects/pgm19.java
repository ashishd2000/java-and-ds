class demo {
	static int x=10 ;
	
	static {
		static int a=10 ;  /*ERROR-illegal start of expression*/
	}

	void fun() {
		static int b=10 ;  /*ERROR-illegal start of expression*/
	}

	static void gun() {
		static int c=10 ;  /*ERROR-illegal start of expression*/
	}
}

