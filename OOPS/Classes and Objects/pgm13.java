class staticDemo {
	static int x=10 ;
	static int y=20 ;
}

/*  After compilation
 *  class staticDemo {
 *  	static int x ;
 *  	static int y ;
 *  	staticDemo() {
 *  		super() ;
 *  		return ;
 *  	}
 *  	static {
 *  		x=10 ;
 *  		y=20 ;
 *  	}
 *
 */

