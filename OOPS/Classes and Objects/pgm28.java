class demo {

	demo() {
		this(50) ;
		System.out.println("No-args constructor") ;
	}

	demo(int x) {
		this() ;  //must be first line
		System.out.println("Para constructor") ;
	}

	public static void main(String[] args) {
		demo obj=new demo(50) ;
	}
}

/* Error-recursive constructor invocation(method call) */
