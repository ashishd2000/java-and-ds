class demo {
	demo() {
		System.out.println("No argument constructor") ;
	}

	demo(int x) { 
		System.out.println("Parameterized constructor") ;
	}

	public static void main(String[] args) {
		demo obj1=new demo() ;
		demo obj2=new demo(10) ;
	}
}
