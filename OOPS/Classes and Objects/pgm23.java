class demo {
	int x=10 ;

	demo() {
		System.out.println(this) ;
		System.out.println(x) ;
	}

	void fun() {
		System.out.println(this) ;
		System.out.println(x) ;
		System.out.println(this.x) ;
	}

	public static void main(String[] args) {
		demo obj=new demo() ;  /* internally  demo(obj)  */
		obj.fun() ;     //fun(obj)
		System.out.println(obj) ;
	}
}
