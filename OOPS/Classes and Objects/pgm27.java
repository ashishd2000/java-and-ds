class demo {
	int x=10 ;

	demo() {
		System.out.println("No-args constructor") ;
	}

	demo(int x) {
		this() ;  //must be first line
		System.out.println("Para constructor") ;
	}

	public static void main(String[] args) {
		demo obj=new demo(50) ;
	}
}


