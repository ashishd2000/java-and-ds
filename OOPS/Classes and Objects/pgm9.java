class courseC2W {
	int topics=8 ;
	private String favC="C Language" ;

	void display() {
		System.out.println(topics) ;
		System.out.println(favC) ;
	}
}
class user {
	public static void main(String[] args) {
		courseC2W x=new courseC2W() ;
		x.display() ;
		System.out.println(x.topics) ;
		System.out.println(x.favC) ; /* ERROR: favC has private
						access in c2w  */

	}
}
