class ConstructorDemo {
	ConstructorDemo () {
		System.out.println("In constructor") ;
	}
	void fun () {
		ConstructorDemo obj=new ConstructorDemo() ;
		System.out.println(obj) ;
	}
	public static void main (String[] args) {
		ConstructorDemo obj1=new ConstructorDemo() ;
		obj1.fun() ;
		System.out.println(obj1) ;

		ConstructorDemo obj2=new ConstructorDemo() ;
		obj2.fun() ;
		System.out.println(obj2) ;
	}
}
		
