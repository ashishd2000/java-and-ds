class player {
	private int jerNo=0 ;
	private String name=null ;

	player(int jerNo,String name) {
		this.jerNo=jerNo ;
		this.name=name ;
	}
	
	void info() {
		System.out.println(jerNo+"="+name) ;
	}
}

class client {
	public static void main(String[] args) {
		player obj1=new player(18,"Virat") ;
		obj1.info() ;
		
		player obj2=new player(18,"MSD") ;
		obj2.info() ;
	}
}
