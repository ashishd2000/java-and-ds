class demo {
	int x=10 ;

	demo() {
		System.out.println("Constructor") ;
	}

	{
		System.out.println("Instance block 1") ;
	}

	public static void main(String[] args) {
		demo obj=new demo() ; 
		System.out.println("Main") ;
	}

	{
		System.out.println("Instance block 2") ;
	}
}

/* 1)static variable
 * 2)static block
 * 3)instance variaable
 * 4)instance block
 * 5)constructor
 * 6)static methods
 * 7)instance methods   */
		


