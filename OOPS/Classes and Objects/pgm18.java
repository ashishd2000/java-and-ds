class demo {
	static {
		System.out.println("Static block 1") ;
	}

	public static void main(String[] args) {
		System.out.println("In demo Main method") ;
	}
}

class client {
	static {
		System.out.println("Static block 2") ;
	}

	public static void main(String[] args) {
		System.out.println("In client Main method") ;
	}


	static {
		System.out.println("Static block 3") ;
	}
}


