class demo {
	int x=10 ;
	static int y=20 ;

	demo() {
		System.out.println("In Constructor") ;
	}

	{
		System.out.println("Instance block 1") ;
	}

	static {
		System.out.println("In static block 1") ;
	}

	public static void main(String[] args) {
		demo obj=new demo() ; 
		System.out.println("Main") ;
	}

	{
		System.out.println("Instance block 2") ;
	}
	
	static {
		System.out.println("In static block 1") ;
	}
}

/* 1)static variable
 * 2)static block
 * 3)instance variaable
 * 4)instance block
 * 5)constructor
 * 6)static methods
 * 7)instance methods   */
		


