class Employee {
	int empId=10 ;              //instance variable seperate for all objects
	String name="Kanha" ;       // --||--

	static int y=50 ;           //static variable common for all objects

	void empInfo() {
		System.out.println(empId) ;
		System.out.println(name) ;
		System.out.println(y) ;
	}
}

class mainDemo {
	public static void main(String[] args) {
		Employee emp1=new Employee() ;
		Employee emp2=new Employee() ;
		
		emp1.empInfo() ;
		emp2.empInfo() ;

		emp2.empId=20 ;
		emp2.name="Rahul" ;
		emp2.y=30 ;
		
		emp1.empInfo() ;
		emp2.empInfo() ;
	}
}
