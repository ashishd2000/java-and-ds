interface demo1 {
	default void fun() {
		System.out.println("demo-1") ;
	}
}
interface demo2 {
	default void fun() {
		System.out.println("demo-2") ;
	}
}
class demochild implements demo1,demo2 {
	public void fun() {
		System.out.println("demo-child") ;
	}
}
class client {
	public static void main(String[] args) {
		demochild obj=new demochild() ;
		obj.fun() ;
		
		demo1 obj1=new demochild() ;
		obj1.fun() ;
		
		demo2 obj2=new demochild() ;
		obj2.fun() ;
	}
}
	
