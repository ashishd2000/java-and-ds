interface demo1 {
	static void m1() {
		System.out.println("demo1-m1") ;
	}
}
interface demo2 {
	static void m1() {
		System.out.println("demo2-m1") ;
	}
}
interface demo3 extends demo1,demo2 {

}
class demochild implements demo3 {
	public static void main(String[] args) {
		demo1 obj=new demochild() ;
		obj.m1() ;
	}
}
