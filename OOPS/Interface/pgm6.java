interface demo {
	static void fun() {   //static--modifier  not-access specifier
		System.out.println("fun") ;
	}
	default void gun() {  //default--modifier  not-access specifier
		System.out.println("gun") ;
	}
}

