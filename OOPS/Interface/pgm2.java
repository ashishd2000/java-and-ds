interface demo {
	void fun() ;
	void gun() ;
}
abstract class demochild implements demo {
	public void gun() {
		System.out.println("gun") ;
	}
} 
class demochild1 extends demochild {
	public void fun() {
		System.out.println("fun") ;
	}
}
class client {
	public static void main(String[] args) {
		demo obj=new demochild1() ;
		obj.fun() ;
		obj.gun() ;
	}
}
