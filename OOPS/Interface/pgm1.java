interface demo {
	void fun() ;
	void gun() ;
}
class demoChild implements demo {
	public void fun() {
		System.out.println("fun") ;
	}
	public void gun() {
		System.out.println("gun") ;
	}
}
class client {
	public static void main(String[] args) {
		demo obj=new demoChild() ;
		obj.fun() ;
		obj.gun() ;
	}
}
