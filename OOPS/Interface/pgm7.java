interface demo {
	void gun() ;

	default void fun() {
		System.out.println("fun demo") ;
	}
}
class demochild implements demo {
	public void gun() {
		System.out.println("gun child") ;
	}
}
class client {
	public static void main(String[] args) {
		demo obj=new demochild() ;
		obj.fun() ;
		obj.gun() ; 
	}
}
