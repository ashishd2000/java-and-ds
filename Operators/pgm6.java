// LOgical Binary operator

class BinaryOp3 {
	public static void main (String [] args) {
		int var1 = 20 ;
		int var2 = 0 ;
		System.out.println((var1>var2) && (var2<var1)) ;  //Returns boolean type data ,True
		System.out.println((var1>var2) || (var2<var1)) ;  //True
		System.out.println((var2>var1) || (var1<var2)) ;  //False
	}

}
