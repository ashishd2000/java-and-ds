class UnaryOp1 {
	public static void main(String[] args){
		int x = 5 ;
		System.out.println(x++ + x++);//x=5 temp=6 , x=6 temp=7  , 11
		System.out.println(x);  //7
		System.out.println(++x + x++);  //x=8 , x=8 temp=9 , 16
		System.out.println(x);  //9
		System.out.println(~x);  // -10
	}
}
