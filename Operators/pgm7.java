// Bitwise binary operator


class BitwiseOp1 {
	public static void main(String [] args) {
		int var1 = 5 ; //0101
		int var2 = 4 ; //0100

		System.out.println(var1 & var2) ; //0100 , 4
		System.out.println(var1 | var2) ; //0101 , 5
		System.out.println(var1 ^ var2) ; //0001 , 1
	}
}
