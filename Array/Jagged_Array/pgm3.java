class demo {
	public static void main (String[] args) {
		int arr[][]=new int[3][] ;      /* Only row size is mandatory in 2d array
						   Here objects of rows are created on heap
						   as the number of rows given braces,
						   Hence we have need to creat objects of column
						   by 'new'   */
/*Error		arr[0]={1,2,3} ;
		arr[0]={4,5} ;
		arr[0]={6} ;           
*/		

		arr[0]=new int[]{1,2,3} ;
		arr[1]=new int[]{4,5} ;
		arr[2]=new int[]{6} ;
	}
}


