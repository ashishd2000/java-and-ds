class demo {
	public static void main(String[] args) {
		int arr1[]=new int[4] ;
		float arr2[]=new float[4] ;
		double arr3[]=new double[4] ;
		boolean arr4[]=new boolean[4] ;
		String arr5[]=new String[4] ;
		demo arr6[]=new demo[4] ;  


		System.out.println("Bydefault values in integer array") ;
		for (int i=0 ; i<4 ; i++) {
			System.out.print("|"+arr1[i]+"| ") ;
		}
		System.out.println() ;
		
		System.out.println("Bydefault values in float array") ;
		for (int i=0 ; i<4 ; i++) {
			System.out.print("|"+arr2[i]+"| ") ;
		}
		System.out.println() ;
		
		System.out.println("Bydefault values in double array") ;
		for (int i=0 ; i<4 ; i++) {
			System.out.print("|"+arr3[i]+"| ") ;
		}
		System.out.println() ;

		System.out.println("Bydefault values in boolean array") ;
		for (int i=0 ; i<4 ; i++) {
			System.out.print("|"+arr4[i]+"| ") ;
		}
		System.out.println() ;

		System.out.println("Bydefault values in string array") ;
		for (int i=0 ; i<4 ; i++) {
			System.out.print("|"+arr5[i]+"| ") ;
		}
		System.out.println() ;  

		System.out.println("Bydefault values in obj array") ;
		for (int i=0 ; i<4 ; i++) {
			System.out.print("|"+arr6[i]+"| ") ;
		}
		System.out.println() ;  
	}
}

