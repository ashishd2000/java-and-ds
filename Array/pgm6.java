import java.io.* ;
class demo {
	public static void main(String[] args)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of array") ;
		int N=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[N] ;
		System.out.println("Enter elements in an array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}
		
		System.out.println("Enter elements in char array") ;
		char arr1[]=new char[N] ;
		for (int i=0 ; i<arr1.length ; i++) {
			arr1[i]=(char)br.read() ;
			br.skip(1) ;
		}

		System.out.println("Elements in an int array are") ;
		for (int i=0 ; i<arr.length ; i++) {
			System.out.print(arr[i]+" ") ;
		}
		System.out.println() ;

		System.out.println("Elements in char array are") ;
		for (int i=0 ; i<arr1.length ; i++) {
			System.out.print(arr1[i]+" ") ;
		}
		System.out.println() ;
		
	}
}

