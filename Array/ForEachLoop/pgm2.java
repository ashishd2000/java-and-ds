class demo {
	public static void main (String args[]) {
		int arr[]={10,20,30,40,50} ;

		/* For Each Loop
		 * We cannot skip any iteration in these loop
		*/

		for (float x : arr) {
			System.out.println(x) ;
		}
	}
}
