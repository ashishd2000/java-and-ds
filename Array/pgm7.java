//Passing an array an Argument

class demo {
	public static void main (String args[]) {
		int arr[]={1,2,3,4,5} ;
	
		fun(arr) ;
	}

	static void fun(int arr[]) {
		for (int a: arr) {
			System.out.println(a) ;
		}
	}
}
