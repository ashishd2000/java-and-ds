
class demo {
	public static void main (String args[]) {
		int arr[]={1,2,3,4,5} ;
		System.out.println("Array elements before passing to any method") ;
		for (int x:arr) {
			System.out.print(x+" ") ;
		}
		System.out.println() ;
	
		fun(arr) ;
		
		System.out.println("Array elements after passing to any method") ;
		for (int x:arr) {
			System.out.print(x+" ") ;
		}
		System.out.println() ;
	}

	static void fun(int arr[]) {
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=arr[i]+50 ;
		}
	}
}
