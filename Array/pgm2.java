class demo {
	public static void main(String[] args) {
		int arr1[]=new int[]{10,20,30,40} ;
		int arr2[]=new int[4] ;   //Bydefault values in int array are 'zero'
		int arr3[]={100,110,120,130} ;   // Internally int arr3[]=new int[]{100,110,120,130}

		arr2[0]=50 ;   // arr2[]={50,0,0,0}
		arr2[1]=60 ;   // arr2[]={50,60,0,0}
		arr2[2]=70 ;   // arr2[]={50,60,70,0}
		arr2[3]=80 ;   // arr2[]={50,60,70,80}
		
		System.out.println(arr2[0]) ;
		System.out.println(arr2[1]) ;
		System.out.println(arr2[2]) ;
		System.out.println(arr2[3]) ;
	}
}
