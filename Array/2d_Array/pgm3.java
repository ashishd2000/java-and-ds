import java.io.* ;
class demo {
	public static void main (String[] args)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter number of rows in array") ;
		int row=Integer.parseInt(br.readLine()) ;
		
		System.out.println("Enter number of columns in array") ;
		int col=Integer.parseInt(br.readLine()) ;

		int arr[][]=new int[row][col] ;

		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			for (int j=0 ; j<arr[i].length ; j++) {
				arr[i][j]=Integer.parseInt(br.readLine()) ;
			}
		}
		
		System.out.println("Elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			for (int j=0 ; j<arr[i].length ; j++) {
				System.out.print(arr[i][j]+" ") ;
			}
			System.out.println() ;
		}
	}
}
