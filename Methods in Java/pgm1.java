class pgm {
	public static void main (String[] args) {
		fun() ;
		gun() ;  //Error: non-static method gun() cannot be referenced from a static context
	}

	static void fun () {
		System.out.println("In fun method") ;
	}

	void gun () {
		System.out.println("In gun method") ;
	}
}
