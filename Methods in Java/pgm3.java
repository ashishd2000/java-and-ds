class pgm {
	int x=10 ;
	static int y=20 ;
	void fun() {
		System.out.println("X="+x) ; /* We can access non static variables or methods
						from non static method */
		System.out.println("Y="+y) ;
	}
	public static void main (String[] args) {
		pgm obj=new pgm() ;
		obj.fun() ;
	}
}
