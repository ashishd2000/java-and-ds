class pgm {
	static byte x = 127 ;
	public static void main (String [] args ) {
		x++ ;
		System.out.println(x) ; /*Byte loop having range from
					  negative 128 to positive 127
					  But when we enter max value than 127
					  it returns back towards -128 */
	}
}
