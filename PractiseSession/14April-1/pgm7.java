/* O
 * 14 13
 * L K J 
 * 9 8 7 6
 * E D C B A       
 *
 *
 * 10
 * I H
 * 7 6 5
 * D C B A      */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		System.out.println("Enter number of rows");
		int x=Integer.parseInt(br.readLine()) ;

		int num=(x*(x+1))/2 ;
		char ch=(char)(64+num) ;
		for (int i=1 ; i<=x ; i++) {
			for (int j=1 ; j<=i ; j++) {
				if (x%2==0) {
					if (i%2==0)
						System.out.print(ch+" ") ;
					else 
						System.out.print(num+" ") ;
				}
				else {
					if (i%2==0)
						System.out.print(num+" ") ;
					else 
						System.out.print(ch+" ") ;
				}
				num-- ;
				ch-- ;
			}
			System.out.println() ;
		}
	}
}

