/* 5 4 3 2 1
 * 8 6 4 2
 * 9 6 3
 * 8 4
 * 5         */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		System.out.println("Enter number of rows");
		int x=Integer.parseInt(br.readLine()) ;

		int num=x ;
		for (int i=1 ; i<=x ; i++) {
			int var1=i*num ;
			for (int j=1 ; j<=x-i+1 ; j++) {
				System.out.print(var1+" ") ;
				var1-=i ;
			}
			System.out.println() ;
			num-- ;
		}
	}
}

