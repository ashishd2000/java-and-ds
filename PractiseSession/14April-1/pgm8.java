/* $
 * @ @
 * & & & 
 * # # # #
 * $ $ $ $ $
 * @ @ @ @ @ @
 * & & & & & & &
 * # # # # # # # #   */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		System.out.println("Enter number of rows");
		int x=Integer.parseInt(br.readLine()) ;

		int r=1 ;
		for (int i=1 ; i<=x ; i++) {
			for (int j=1 ; j<=i ; j++) {
				if (r==1)
					System.out.print("$ ") ;
				if (r==2)
					System.out.print("@ ") ;
				if (r==3)
					System.out.print("& ") ;
				if (r==4)
					System.out.print("# ") ;
			}
			System.out.println() ;
			r++ ;
			if (r==5)
				r=1 ;
		}
	}
}
