/* Even numbers in reverse
 * Odd numbers in standard way  */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		System.out.println("Enter starting of range");
		int start=Integer.parseInt(br.readLine()) ;
		System.out.println("Enter end of range");
		int end=Integer.parseInt(br.readLine()) ;

		for (int i=end ; i>=start ; i--) {
			if (i%2==0)
				System.out.print(i+" ") ;
		}
		System.out.println() ;

		for (int i=start ; i<=end ; i++) {
			if (i%2!=0)
				System.out.print(i+" ") ;
		}
		System.out.println() ;
	}
}
