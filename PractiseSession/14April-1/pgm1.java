/*  D4 C3 B2 A1
 *  A1 B2 C3 D4
 *  D4 C3 B2 A1
 *  A1 B2 C3 D4   */
import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		System.out.println("Enter number of rows");
		int x=Integer.parseInt(br.readLine()) ;

		char ch ;
		int num ;
		for (int i=1 ; i<=x ; i++) {
			if (i%2==0) {
				ch='A' ;
				num=1 ;
			}
			else {
				ch=(char)(64+x) ;
				num=x ;
			}
			for (int j=1 ; j<=x ; j++) {
				if (i%2==0) {
					System.out.print(ch) ;
					System.out.print(num+" ") ;
					ch++ ;
					num++ ;
				}
				else {
					System.out.print(ch) ;
					System.out.print(num+" ") ;
					ch-- ;
					num-- ;
				}
			}
			System.out.println() ;
		}
	}
}

