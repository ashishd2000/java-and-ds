/* # = = =
 * = # = =
 * = = # =
 * = = = #    */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		System.out.println("Enter number of rows");
		int x=Integer.parseInt(br.readLine()) ;

		for (int i=1 ; i<=x ; i++) {
			for (int j=1 ; j<=x ; j++) {
				if (i==j) 
					System.out.print("# ") ;
				else 
					System.out.print("= ") ;
			}
			System.out.println() ;
		}
	}
}
