// Sum of factorials in number  

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		System.out.println("Enter number");
		int num=Integer.parseInt(br.readLine()) ;

		int sum=0 ;
		while (num!=0) {
			int rem=num%10 ;
			int mul=1 ;
			while (rem!=1) {
				mul=mul*rem ;
				rem-- ;
			}
			sum+=mul ;
			num/=10 ;
		}
		System.out.println("Sum="+sum) ;
	}
}


		
