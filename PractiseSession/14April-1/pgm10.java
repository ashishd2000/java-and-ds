// Prime numbers in range

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		
		System.out.println("Enter starting of range");
		int start=Integer.parseInt(br.readLine()) ;
		System.out.println("Enter end of range");
		int end=Integer.parseInt(br.readLine()) ;

		for (int i=start ; i<=end ; i++) {
			int flag=0 ;
			if (i==1) 
				flag=0 ;
			else {
				for (int j=2 ; j<=i/2 ; j++) {
					if (i%j==0) {
						flag=1 ;
						break ;
					}
				}
				if (flag==0)
					System.out.print(i+" ") ;
			}
		}
		System.out.println() ;
	}
}
