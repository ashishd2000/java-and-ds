//anagram string

import java.io.* ;
class demo {
	static boolean anagram (String str1,String str2) {
		char arr1[]=str1.toCharArray() ;
		char arr2[]=str2.toCharArray() ;

		int i=0 ;
		while (i!=arr1.length) {
			int cnt1=0 ;
			int cnt2=0 ;
			char ch=arr1[i] ;
			int j=0 ;
			while (j!=arr1.length) {
				if (arr1[j]==ch)
					cnt1++ ;
				if (arr2[j]==ch)
					cnt2++ ;
				j++ ;
			}
			if (cnt1!=cnt2)
				return false ;
			i++ ;
		}
		return true ;
	}

	public static void main (String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
	
		System.out.println("Enter first string") ;
		String str1=br.readLine() ;
		
		System.out.println("Enter second string") ;
		String str2=br.readLine() ;

		boolean ret=anagram(str1,str2) ;
		if (ret==true)
			System.out.println("Anagram") ;
		else 
			System.out.println("Non Anagram") ;
	}
}

	

		
