//palindrome string

import java.io.* ;
class demo {
	static boolean palindrome (String str) {
		char arr[]=str.toCharArray() ;

		int i=0 ; 
		int j=arr.length-1 ;

		while (i<j) {
			if (arr[i]!=arr[j]) 
				return false ;
			i++ ;
			j-- ;
		}
		return true ;
	}

	public static void main (String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
	
		System.out.println("Enter any string") ;
		String str=br.readLine() ;

		boolean ret=palindrome(str) ;
		if (ret==true)
			System.out.println("Palindrome") ;
		else 
			System.out.println("Non Palindrome") ;
	}
}

	

		
