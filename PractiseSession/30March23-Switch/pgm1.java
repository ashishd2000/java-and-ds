/* Write a program in which students should enter marks of 5 different subjects. 
 * If all subject having above passing marks add them and provide to switch case to 
 * print grades(first class second class), if student get fail in any subject program 
 * should print "You failed in exam            */


import java.io.* ;
class pgm {
	public static void main (String[] args)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter your five subject marks") ;
		int sub1=Integer.parseInt(br.readLine()) ;
		int sub2=Integer.parseInt(br.readLine()) ;
		int sub3=Integer.parseInt(br.readLine()) ;
		int sub4=Integer.parseInt(br.readLine()) ;
		int sub5=Integer.parseInt(br.readLine()) ;

		int sum=sub1+sub2+sub3+sub4+sub5 ;

		switch (sum) {
			case 500 :
				System.out.println("First class") ;
				break ;
			case 400 :
			       System.out.println("Second class") ;
			       break ;
			case 300 :
			       System.out.println("Pass") ;
			       break ;
			default :
			       System.out.println("Failed in exam") ;
		}
	}
}
		       			       
