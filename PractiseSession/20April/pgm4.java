/* WAP to search a specific element from an array and return its index.
Input: 1 2 4 5 6
Enter element to search: 4
Output: element found at index: 2                     */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in an array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}
		int searchI=-1 ;
		System.out.println("Enter element want to search") ;
		int search=Integer.parseInt(br.readLine()) ;
		for (int i=0 ; i<arr.length ; i++) {
			if (search==arr[i]) {
				searchI=i ;
				break ;
			}
		}
		if (searchI!=-1)
			System.out.println(search+" found at index "+searchI) ;
		else 
			System.out.println(search+" not found any index") ;

	}
}

