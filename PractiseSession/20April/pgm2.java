/* WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
Number of Even Elements: 4
Number of Odd Elements : 3                        */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		int eCount=0 ;
		System.out.println("Enter elements in an array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
			if (arr[i]%2==0)
				eCount++ ;
		}
		System.out.println("Total even numbers in array="+eCount) ;
		System.out.println("Total odd numbers in array="+(arr.length-eCount)) ;
	}
}

