/* Program 10
WAP to print the elements whose addition of digits is even.
Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
Input :
Enter array : 1 2 3 5 15 16 14 28 17 29 123
Output: 2 15 28 17 123                    */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in first array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}
		
		for (int i=0 ; i<arr.length ; i++) {
			int num=arr[i] ;
			int sum=0 ;
			while (num!=0) {
				int rem=num%10 ;
				sum=sum+rem ;
				num/=10 ;
			}
			if (sum%2==0)
				System.out.print(arr[i]+" ") ;
		}
		System.out.println() ;
	}
}

