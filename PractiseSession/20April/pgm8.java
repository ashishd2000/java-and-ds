/* WAP to find the uncommon elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Uncommon elements :
3
5
9
8                    */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr1[]=new int[n] ;
		System.out.println("Enter elements in first array") ;
		for (int i=0 ; i<arr1.length ; i++) {
			arr1[i]=Integer.parseInt(br.readLine()) ;
		}
		
		int arr2[]=new int[n] ;
		System.out.println("Enter elements in second array") ;
		for (int i=0 ; i<arr2.length ; i++) {
			arr2[i]=Integer.parseInt(br.readLine()) ;
		}

		for (int i=0 ; i<arr1.length ; i++) {
			int flag=0 ;
			for (int j=0 ; j<arr2.length ; j++) {
				if (arr1[i]==arr2[j]) {
					flag=1 ;
					break ;
				}
			}
			if (flag==0) 
				System.out.print(arr1[i]+" ") ;
		}
		
		for (int i=0 ; i<arr2.length ; i++) {
			int flag=0 ;
			for (int j=0 ; j<arr1.length ; j++) {
				if (arr2[i]==arr1[j]) {
					flag=1 ;
					break ;
				}
			}
			if (flag==0) 
				System.out.print(arr2[i]+" ") ;
		}
		System.out.println() ;
	}
}

