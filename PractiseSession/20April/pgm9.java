/* Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array                    */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of first array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr1[]=new int[n] ;
		System.out.println("Enter elements in first array") ;
		for (int i=0 ; i<arr1.length ; i++) {
			arr1[i]=Integer.parseInt(br.readLine()) ;
		}
		
		System.out.println("Enter size of second array") ;
		int m=Integer.parseInt(br.readLine()) ;
		int arr2[]=new int[m] ;
		System.out.println("Enter elements in second array") ;
		for (int i=0 ; i<arr2.length ; i++) {
			arr2[i]=Integer.parseInt(br.readLine()) ;
		}

		
		int mergeA[]=new int[(m+n)] ;
		for (int i=0 ; i<arr1.length ; i++) {
			mergeA[i]=arr1[i] ;
		}
		int index=arr1.length ;
		for (int i=0 ; i<arr2.length ; i++) {
			mergeA[index]=arr2[i] ;
			index++ ;
		}

		System.out.println("Merged array is") ;
		for (int i=0 ; i<mergeA.length ; i++)
			System.out.print(mergeA[i]+" ") ;

		System.out.println() ;
	}
}

