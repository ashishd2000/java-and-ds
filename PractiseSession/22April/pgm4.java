/*WAP to find a prime number from an array and return its index.
Take size and elements from the user
Input: 10 25 36 566 34 53 50 100
Output: prime no 53 found at index: 5i                */

import java.io.* ;
class demo {
	boolean primeN (int num) {
		int cnt=0 ;
		for (int i=2 ; i<=num/2 ; i++) {
			if (num%i==0) {
				cnt++ ;
			}
		}
		if (cnt==0 && num!=1) 
			return true ;
		return false ;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		demo obj=new demo() ;
		System.out.println("Prime numbers in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			boolean ret=obj.primeN(arr[i]) ;
			if (ret==true)
				System.out.println(arr[i]+" is a prime number found at index "+i) ;
		}
	}
}
