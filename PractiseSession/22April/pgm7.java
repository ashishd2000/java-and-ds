/*WAP to find a Strong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564 145
Output: Strong no 145 found at index: 5                */

import java.io.* ;
class demo {
	int strongN (int num) {
		int sum=0 ;
		while (num!=0) {
			int rem=num%10 ;
			int mul=1 ;
			while (rem!=1) {
				mul=mul*rem ;
				rem-- ;
			}
			sum+=mul ;
			num/=10 ;
		}
		return sum ;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		demo obj=new demo() ;
		System.out.println("Strong numbers in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			int ret=obj.strongN(arr[i]) ;
			if (ret==arr[i])
				System.out.println(arr[i]+" is a strong number found at index "+i) ;
		}
	}
}
