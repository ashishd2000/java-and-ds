/*Program 10
Write a program to print the second min element in the array
Input: Enter array elements: 255 2 1554 15 65 95 89
Output: 15             */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		int min=arr[0] ;
		int max=arr[0] ;
		for (int i=1 ; i<arr.length ; i++) {
			if (min>arr[i])
				min=arr[i] ;
			if (max<arr[i])
				max=arr[i] ;
		}
		
		int sMin=max ;
		for (int i=1 ; i<arr.length ; i++) {
			if (arr[i]>min && arr[i]<sMin) 
				sMin=arr[i] ;
				
		}
		System.out.println("Second min element in array is "+sMin) ;
	}
}
