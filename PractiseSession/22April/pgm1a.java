/*Program 1
Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4 */

import java.io.* ;
class demo {
	int count (String str) {
		return str.length() ;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		String str[]=new String[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			str[i]=br.readLine() ;
		}

		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(str[i]) ;
		}

		demo obj=new demo() ;
		System.out.println("Count of digits in elements of array") ;
		for (int i=0 ; i<arr.length ; i++) {
			int cnt=obj.count(str[i]) ;
			System.out.print(cnt+" ") ;
		}
		System.out.println() ;
	}
}
