/*Program 8
WAP to find an ArmStong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 153 55 89
Output: Armstrong no 153 found at index: 4             */

import java.io.* ;
class demo {
	int count (int num) {
		int cnt=0 ;
		while (num!=0) {
			cnt++ ;
			num/=10 ;
		}
		return cnt ;
	}
	int armstrongN (int num) {
		int cnt=count(num) ;
		int sum=0 ;
		while (num!=0) {
			int rem=num%10 ;
			int mul=1 ;
			int cnt1=cnt ;
			while (cnt1!=0) {
				mul=mul*rem ;
				cnt1-- ;
			}
			sum+=mul ;
			num/=10 ;
		}
		return sum ;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		demo obj=new demo() ;
		System.out.println("Armstrong numbers in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			int ret=obj.armstrongN(arr[i]) ;
			if (ret==arr[i])
				System.out.println(arr[i]+" is a armstrong number found at index "+i) ;
		}
	}
}
