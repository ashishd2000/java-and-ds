/*Program 6
WAP to find a palindrome number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564

Output: Palindrome no 252 found at index: 2                */

import java.io.* ;
class demo {
	int palindromeN (int num) {
		int rev=0 ;
		while (num!=0) {
			rev=rev*10+num%10 ;
			num/=10 ;
		}
		return rev ;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		demo obj=new demo() ;
		System.out.println("Palindrome numbers in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			int ret=obj.palindromeN(arr[i]) ;
			if (ret==arr[i])
				System.out.println(arr[i]+" is a palindrome number found at index "+i) ;
		}
	}
}
