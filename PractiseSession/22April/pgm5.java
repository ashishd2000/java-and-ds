/*Program 5
WAP to find a Perfect number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 496 564
Output: Perfect no 496 found at index: 3                */

import java.io.* ;
class demo {
	int perfectN (int num) {
		int sum=0 ;
		for (int i=1 ; i<=num/2 ; i++) {
			if (num%i==0) {
				sum+=i ;
			}
		}
		return sum ;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		demo obj=new demo() ;
		System.out.println("Perfect numbers in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			int ret=obj.perfectN(arr[i]) ;
			if (ret==arr[i])
				System.out.println(arr[i]+" is a perfect number found at index "+i) ;
		}
	}
}
