/*Program 9
Write a program to print the second max element in the array
Input: Enter array elements: 2 255 2 1554 15 65
Output: 255             */

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		int max=arr[0] ;
		for (int i=1 ; i<arr.length ; i++) {
			if (max<arr[i])
				max=arr[i] ;
		}

		int second_max=arr[0] ;
		for (int i=0 ; i<arr.length ; i++) {
			if (arr[i]<max && second_max<arr[i])
				second_max=arr[i] ;
		}
		System.out.println("Second max element in array is "+second_max) ;
	}
}
