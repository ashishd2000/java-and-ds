/*WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465                        */

import java.io.* ;
class demo {
	int reverseN (int num) {
		int rev=0 ;
		while (num!=0) {
			rev=rev*10+(num%10) ;
			num/=10 ;
		}
		return rev ;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in  array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		demo obj=new demo() ;
		System.out.println("Reverse numbers in elements of array") ;
		for (int i=0 ; i<arr.length ; i++) {
			int rev=obj.reverseN(arr[i]) ;
			System.out.print(rev+" ") ;
		}
		System.out.println() ;
	}
}
