/*Program 3
WAP to find a composite number from an array and return its index.
Take size and elements from the user
Input: 1 2 3 5 6 7
Output: composite 6 found at index: 4                    */

import java.io.* ;
class demo {
	boolean compositeN (int num) {
		for (int i=2 ; i<=num/2 ; i++) {
			if (num%i==0) {
				return true ;
			}
		}
		return false ;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}

		demo obj=new demo() ;
		System.out.println("Composite numbers in array") ;
		for (int i=0 ; i<arr.length ; i++) {
			boolean ret=obj.compositeN(arr[i]) ;
			if (ret==true)
				System.out.println(arr[i]+" is a composite number found at index "+i) ;
		}
	}
}
