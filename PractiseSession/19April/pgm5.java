//WAP,take input array from user and print only elements that are divisible by 5

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		System.out.println("Enter elements in an array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
		}
		for (int i=0 ; i<arr.length ; i++) {
			if (arr[i]%5==0)
				System.out.print(arr[i]+" ") ;
		}
		System.out.println() ;
	}
}

