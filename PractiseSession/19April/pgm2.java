//Multiplication of even numbers in array

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		int arr[]=new int[n] ;
		int mul=1 ;
		System.out.println("Enter elements in an array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=Integer.parseInt(br.readLine()) ;
			if (arr[i]%2==0)
				mul=mul*arr[i] ;
		}
		System.out.println("Multiplication of even numbers in array is="+mul) ;
	}
}

