//WAP,take charcter array from user,print only vowels from the array

import java.io.* ;
class demo {
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter size of an array") ;
		int n=Integer.parseInt(br.readLine()) ;

		char arr[]=new char[n] ;
		System.out.println("Enter elements in an array") ;
		for (int i=0 ; i<arr.length ; i++) {
			arr[i]=br.readLine().charAt(0) ;
		}

	
		for (int i=0 ; i<arr.length ; i++) {
			if (arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u' || arr[i]=='A' || arr[i]=='E' || arr[i]=='I' || arr[i]=='O' || arr[i]=='U')
				System.out.print(arr[i]+" ") ;
		}
		System.out.println() ;
	}
}

