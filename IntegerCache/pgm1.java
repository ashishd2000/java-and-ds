class demo {
	public static void main (String args[]) {
		int x=10 ;
		int y=10 ;		      /* The integer and charcter values between -128 to +127
						 get memory in heap section in an Integer cache
						 If many variables having same values then they all 
						 variable are connected with a single box having that 
						 value in Integer cache..
						 If we change the value in any box then another (new)
						 box will be generated in Integer cache      */

		Integer z=new Integer(10) ;  /* Here also variable z having same value but due to new 
						here compulsory new obj will be generated */

		System.out.println(System.identityHashCode(x)) ;
		System.out.println(System.identityHashCode(y)) ;
		System.out.println(System.identityHashCode(z)) ;

		char ch1='a' ;
		char ch2='a' ;
		System.out.println(System.identityHashCode(ch1)) ;
		System.out.println(System.identityHashCode(ch2)) ;
	}
}
