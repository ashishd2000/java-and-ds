/*  1 4 9 16
 *    1 4 9
 *      1 4
 *        1    */
class Pattern10 {
	public static void main(String [] args) {
		for (int i=1 ; i<=4 ; i++) {
			int num=1 ;
			for (int j=1 ; j<i ; j++) {
				System.out.print("**") ;
			}
			for (int k=4 ; k>=i ; k--) {
				System.out.print(num*num+" ") ;
				num++ ;
			}
			System.out.println() ;
		}
	}
}
