/*         1
 *       1 4
 *     1 4 9
 *   1 4 9 16      */


class Pattern6 {
	public static void main (String [] args) {
		for (int i=1 ; i<=4 ; i++) {
	
			for (int j=4 ; j>i ; j--) {
				System.out.print("**") ;
			}
			int num=1 ;
			for (int k=1 ; k<=i ; k++) {
				System.out.print(num*num+" ") ;
				num++ ;
			}
			System.out.println() ;
		}

	}
}
