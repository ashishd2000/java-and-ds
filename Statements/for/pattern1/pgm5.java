/*           A
 *         A B
 *       A B C
 *     A B C D   */


class Pattern5 {
	public static void main(String [] args) {
		for (int i=1 ; i<=4 ; i++) {
			char ch='A' ;
			for (int j=4 ; j>i ; j--) {
				System.out.print("**") ;
			}
			for (int k=1 ; k<=i ; k++) {
				System.out.print(ch+" ") ;
				ch++ ;
			}
			System.out.println() ;
		}
	}
}
