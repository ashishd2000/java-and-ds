//Pattern using only one for loop


/* 1
 * 2 3
 * 4 5 6
 * 7 8 9 10   */

class Pattern1 {
	public static void main(String [] args) {
		int num=1 ;
		int j=1 ;
		for (int i=1 ; i<5 ;) {
			
			if (j<i) {
				j++ ;
				System.out.print(num+" ") ;
				num++ ;
				continue ;
			}
			else if (j==i) {
				System.out.print(num+" ") ;
				System.out.println() ;
				j=1 ;
				i++ ;
				num++ ;

			}
			
		}
	}
}
