/* 1 4 3
   16 5 36
   7 64 9 */

class forloop6 {
	public static void main (String [] args) {
		int num = 1 ;
		
		for(int i=1 ; i<=3 ; i++) {
			for(int j=1 ; j<=3 ; j++) {
				
				if(num%2==1) {

					System.out.print(num+ " ") ;
				}
				else {
					
					System.out.print(num*num+ " ") ;
				}
				num++ ;
			
			}
			
		System.out.println() ;
		}
	}
}
