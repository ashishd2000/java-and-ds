/* 4 3 2 1 
 * 3 2 1 
 * 2 1
 * 1       */

import java.util.*;



class Pattern12 {
	public static void main (String [] args) {
		for (int i=0 ; i<4 ; i++) {
	
			for (int j=4 ; j>i ; j--) {
				int num=j-i ;
				System.out.print(num+ " ") ;
				
			}
			System.out.println() ;
		}
	}
}



class Pattern12a {
	public static void main (String [] args) {
	for (int i=4 ; i>=1 ; i--) {
		int x=i ;
		for (int j=x ; j>=1 ; j--) {
			System.out.print(x+ " ") ;
			x-- ;
		}
		System.out.println() ;
	}
}
}






class Pattern12b {
	public static void main(String [] args) {
		
		for (int i=1 ; i<=4 ; i++) {
			
			for(int j=4 ; j>=i ; j--) {
				int num=j+1-i ;
				
					System.out.print(num+ " ") ;
					
				
			}System.out.println() ;
		}
	}
}

class pattern12c{
	public static void main(String args[]){
		Scanner sc  = new Scanner(System.in);
		int n = sc.nextInt();
		
		for (int i = 0;i<n;i++){
			
			for(int j = n-i;j>=1;j--){
				System.out.print(j);
			}
			System.out.println();
		}

	}


}
