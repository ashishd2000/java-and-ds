/* A B C D
 * 1 2 3
 * A B
 * 1      */

class Pattern15 {
	public static void main(String [] args) {
	
		
		for (int i=0 ; i<4 ; i++) {
			int num=1 ;
			char ch1='A' ;
			
			for (int j=4 ; j>i ; j--) {
			
				if(i%2==0) {
					System.out.print(ch1+ " ") ;
					ch1++ ;
				} 
				 if(i%2==1) {
					System.out.print(num+ " ") ;
					num++ ;
				}
			}
			System.out.println() ;
		}
	}
}




class Pattern15a {
	public static void main(String [] args) {
	
		
		for (int i=1 ; i<=4 ; i++) {
			int num=1 ;
			char ch1='A' ;
			
			for (int j=4 ; j>=i ; j--) {
			
				if(i%2==1) {
					System.out.print(ch1+ " ") ;
					ch1++ ;
				} 
				else if(i%2==0) {
					System.out.print(num+ " ") ;
					num++ ;
				}
			}
			System.out.println() ;
		}
	}
}



