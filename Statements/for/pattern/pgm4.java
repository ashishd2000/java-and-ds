/* A
 * A B
 * A B C
 * A B C D */
 
class Pattern4 {
	public static void main (String [] args) {
		for (int i=1  ; i<=4 ; i++) {
			char ch1='A' ;
			for (int j=1 ; j<=i ; j++) {
				System.out.print(ch1+ " ") ;
				ch1++ ;
			}
			System.out.println() ;
		}
	}
}
