/* A B C D
 * 1 2 3
 * E F
 * 4       */

class Pattern14 {
	public static void main(String [] args) {
		char ch1='A' ;
		int num=1 ;
		for (int i=1 ; i<=4 ; i++) {
			for (int j=4 ; j>=i ; j--) {
				if(i%2==1) {
					System.out.print(ch1+ " ") ;
					ch1++ ;
				}
				else if(i%2==0) {
					System.out.print(num+ " ") ;
					num++ ;
				}
			}
			System.out.println() ;
		}
	}
}


