/* ABC
   DEF
   GHI */

class forloop3 {
	public static void main(String [] args) {
		char ch1 = 'A' ;

		for (int i=1 ; i<=3 ; i++) {
			for (int j=1 ; j<=3 ; j++) {
				System.out.print(ch1+" ") ;
				ch1++ ;
			}
		System.out.println() ;
		}
	}
}
