class else_ifP2 {
	public static void main (String [] args) {
		int x = 100 ;
		if (x%2==0) {
			System.out.println(x+" is divisible by 2");
		}
		else {
			System.out.println(x+" is not divisible by 2");
		}
	}
}
