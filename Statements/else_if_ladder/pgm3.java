class else_ifP3 {
	public static void main (String [] args) {
		int x = 20 ;
		if (x%2==0 && x%5==0) {
			System.out.println(x+" is divisible by 2 and 5") ;
		}
		else {
			System.out.println(x+" is not divisible by 2 or 5") ;
		}
	}
}
