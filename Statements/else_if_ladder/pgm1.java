class else_ifP1 {
	public static void main(String [] args) {
		boolean first = false ;
		boolean second = false ;
		boolean third = true ;

		if (first) {
			System.out.println("C2W") ;
		}
		else if (second) {
			System.out.println("C2WF") ;
		}
		else if (third) {
			System.out.println("C2WFamily") ;
		}
		else {
			System.out.println("Try Again") ;
		}
	}
}

			
