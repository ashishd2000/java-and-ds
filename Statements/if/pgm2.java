// if statement

class ifP2 {
	public static void main (String[] args) {
		int x = 10 ;
		
		if (x) {  //Compulsory should be true or false  i.e.boolean value
			  //Error : int cant be converted to boolean value
			System.out.println("In first block");
		}
	System.out.println("In main method");
	}
}
