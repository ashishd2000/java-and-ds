// Only int value is allowable in switch statement

class switchDemo1 {
	public static void main (String [] arga) {
		int x = 4 ;
		/*switch() {
		}  ERROR-illegal start of equation 
		         Expression is compulsory in switch */

		/*switch(x==4) {
		} ERROR-boolean value cant be converted to int */

		switch(x) {
			case 1 :
				System.out.println("ONE");
				break ;
			
			case 2 :
				System.out.println("TWO");
				break ;
			
			case 3 :
				System.out.println("THREE");
				break ;
			
			case 4 :
				System.out.println("FOUR");
				break ;
			
			default :
				System.out.println("In Default");
				break ;
		}
	}
}
