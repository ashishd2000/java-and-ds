
class switchDemo2 {
	public static void main (String [] args) {
		char ch = 'A' ;
		switch (ch) {
			case 'A' :
				System.out.println("Char A") ;
				break ;

			case 'B' :
				System.out.println("Char B") ;
				break ;			

			case 'C' :
				System.out.println("Char C") ;
				break ;

/*			case 65  : ASCII value of 'A' ERROR - duplicate case label
				System.out.println("Char A") ;
				break ; */

			default :
				System.out.println("Default") ;
		}
	}
}

