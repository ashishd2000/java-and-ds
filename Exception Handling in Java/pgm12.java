import java.util.* ;
class DataOverFlowException extends RuntimeException {
	DataOverFlowException(String msg) {
		super(msg) ;
	}
}
class DataUnderFlowException extends RuntimeException {
	DataUnderFlowException(String msg) {
		super(msg) ;
	}
}
class arrayDemo {
	public static void main(String[] args) {
		int arr[]=new int[5] ;
		Scanner sc=new Scanner(System.in) ;

		System.out.println("Enter int value") ;
		System.out.println("0<data<100") ;

		for (int i=0 ; i<arr.length ; i++) {
			int data=sc.nextInt() ;
			
			if(data<0) {
				throw new DataUnderFlowException("Data less than zero") ;
			}
			if(data>100) {
				throw new DataOverFlowException("Data greater than 100") ;
			}

			arr[i]=data ;
		}

		for (int i=0 ; i<arr.length ; i++) {
			System.out.println(arr[i]+" ") ;
		}
	}
}

