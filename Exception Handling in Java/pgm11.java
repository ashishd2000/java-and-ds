/* User defined exceptio
 * throw                 */

import java.util.* ;
class demo {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in) ;

		int x=sc.nextInt() ;

		try {
			if(x==0) {
				throw new ArithmeticException("Divide by zero") ;
			}
			System.out.println(10/x) ;
		}catch(ArithmeticException obj) {
			System.out.print("Exception in thread "+Thread.currentThread().getName()+" ") ;
			obj.printStackTrace() ;
		}
	}
}
