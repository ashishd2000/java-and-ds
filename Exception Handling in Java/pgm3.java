//ArrayIndexOutOfBound Exception

class arrayDemo {
	public static void main(String[] args) {
		int arr[]=new int[] {1,2,3,4,5} ;

		for (int i=0 ; i<=arr.length ; i++) {
			System.out.println(arr[i]) ;
		}
	}
}

/* output
  1
  2
  3
  4
  5
  Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 5 out of bounds for length 5
          at arrayDemo.main(pgm3.java:8)  */
