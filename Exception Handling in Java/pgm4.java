/* Default exception handler-run time
 * 1)thread name
 * 2)exception name 
 * 3)description
 * 4)stact trace                      */

//Arithmetic exception

class demo {
	void m1() {
		System.out.println(10/0) ;  /* Exception
					       Exception in thread "main" java.lang.ArithmeticException: / by zero
				               at demo.m1(pgm4.java:9)
                                               at demo.main(pgm4.java:17)   */
		m2() ;
	}
	void m2() {
		System.out.println("In m2") ;
	}
	public static void main(String[] args) {
		demo obj=new demo() ;
		obj.m1() ;
		System.out.println("End main") ;
	}
}


