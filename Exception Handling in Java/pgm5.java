//NullPointerException
class demo {
	void m1() {
		System.out.println("In m1") ;
	}
	
	void m2() {
		System.out.println("In m2") ;
	}

	public static void main(String[] args) {
		demo obj=new demo() ;
		System.out.println("Start main") ;
		obj.m1() ;
		obj=null ;
		obj.m2() ;
		System.out.println("End main") ;
	}
}
