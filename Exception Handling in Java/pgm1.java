/*          Object
 *              1)throwable
 *                  A)ERROR
 *                      a)jvm
 *                      b)virtual machine error
 *                  B)Exception
 *                      1)Checked (Compile time exception)
 *                         a)IOException
 *                         b)FileNotFoundException
 *                      2)Unchecked (Runtime exception)
 *                         a)ArithmeticException
 *                         b)NullPointerException
 *                         c)IndexOutOfBound
 *                            A)ArrayIndexOutOfBound
 *                            B)StringIndexOutOfBound          */
