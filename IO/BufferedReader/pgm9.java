import java.io.* ;    // If not written---Gives Error for BufferedReader and InputStreamReader

class pgm {
	public static void main (String args[]) throws IOException { /* If 'import io.java.*' written
								        and 'throws IOException' not written then
								        gives error for readLine() method 
									Error-Unreported exception IOException must be caugh
									or declared to be thrown */
		BufferedReader br1=new BufferedReader(new InputStreamReader(System.in)) ;
		BufferedReader br2=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.print("Enter first string\n") ;
		String str1=br1.readLine() ;
		System.out.println("First string "+str1) ;
		
		br1.close() ;  

		System.out.print("Enter second string\n") ;
		String str2=br2.readLine() ;   /*Error- Exception in thread "main" java.io.IOException: Stream closed
						*/
		System.out.println("Second string "+str2) ;
	}
}


