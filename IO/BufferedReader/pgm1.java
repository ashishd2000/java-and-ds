// InputStreamReader is from java io package
 
import java.io.* ;

class pgm {
	public static void main (String[] args)throws IOException {

		InputStreamReader isr=new InputStreamReader(System.in) ;
		/* InputStreamReader is can take single char from user
		 * But it is in 'int' datatype format
		 * To convert it into char we need to typecast it   */

		System.out.println("Enter char") ;	
		int val=isr.read() ;
		System.out.println(val) ;
	}
}

