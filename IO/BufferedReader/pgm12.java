//StringTokenizer (util pakage)

import java.io.* ;
import java.util.* ;

class pgm {
	public static void main (String args[])throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;

		System.out.println("Enter your Movie name,Block in theater,Number of tickets And rating of movie (Seprating with comma between them)") ;
		String mInfo=br.readLine() ;

		StringTokenizer st=new StringTokenizer(mInfo,",") ;

		String mName=st.nextToken() ;
		String str=st.nextToken() ;
		char block=str.charAt(0) ;
		int nTick=Integer.parseInt(st.nextToken()) ;
		float rating=Float.parseFloat(st.nextToken()) ;

		System.out.println("Movie name="+mName) ;
		System.out.println("Block="+block) ;
		System.out.println("Number of tickets="+nTick) ;
		System.out.println("Rating of movie="+rating) ; 
	}
}

