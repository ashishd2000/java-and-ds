/* Taking input from user by using 
 * Scanner class
 * Scanner class is included in java from 1.5 version
 * Scanner class in from java util package       */

import java.util.Scanner ;

class ScannerDemo {
	public static void main (String[] args) {
		Scanner obj=new Scanner(System.in) ;
		System.out.println("Enter u r name") ;	
		String str=obj.next() ;   //Taking name before space
		System.out.println(str) ;	
	}
}
