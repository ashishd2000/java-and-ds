class demo {
	public static void main (String[] args) {
		String str1="Shashi" ;
		String str2="Bagal" ;
		String str3=str1+str2 ;  /* Internally calls to append method 
					    from String builder and return new String */

		String str4=str1.concat(str3) ;
	}
}

