class demo {
	public static void main (String[] args) {
		String str1="Kanha" ;  // String Constant Pool (Heap)
		String str2="Kanha" ;  // String Constant Pool (Heap)
		String str3=new String("Kanha") ; // Heap
		String str4=new String("Kanha") ; // Heap
		String str5="Rahul" ;  //String Constant pool (Heap)
		String str6=new String("Rahul") ;  //Heap
						     
		System.out.println(System.identityHashCode(str1)) ;  // 1000
		System.out.println(System.identityHashCode(str2)) ;  // 1000
		System.out.println(System.identityHashCode(str3)) ;  // 2000   
		System.out.println(System.identityHashCode(str4)) ;  // 3000
		System.out.println(System.identityHashCode(str5)) ;  // 4000
		System.out.println(System.identityHashCode(str6)) ;  // 5000

	}
}
