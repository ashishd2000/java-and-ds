class demo {
	public static void main (String[] args) {
		String str1="Shashi" ;
		String str2="Bagal" ;

		System.out.println(str1) ;  // Shashi
		System.out.println(str2) ;  // Bagal
					    
		str1.concat(str2) ;  // create new object on heap
		
		System.out.println(str1) ;  // Shashi
		System.out.println(str2) ;  // Bagal
	}
}

