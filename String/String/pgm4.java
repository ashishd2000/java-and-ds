class demo {
	public static void main (String[] args) {
		String str1="Ashish" ;  // string constant pool (heap)
		String str2="Salunke" ;  //string constant pool (heap)

		System.out.println(str1+str2) ;  // AshishSalunke

		String str3="AshishSalunke" ;  // string constant pool
		String str4=str1+str2 ;  // heap internally calls to 'new String'
		String str5=str1.concat(str2) ;  // heap internally calls to 'new String'

		System.out.println(System.identityHashCode(str1)) ;  // 1000
		System.out.println(System.identityHashCode(str2)) ;  // 2000
		System.out.println(System.identityHashCode(str3)) ;  // 3000
		System.out.println(System.identityHashCode(str4)) ;  // 4000
		System.out.println(System.identityHashCode(str4)) ;  // 5000
	}
}
