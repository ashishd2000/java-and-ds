/* Method 4:-public int compareTo(String str2)
 * Description:-It compares the str1 and str2
 * (case sensitive),if both the strings are equal
 * it returns 0 otherwise returns the comparision
 * If both strings are equal returns 0
 * If strings are not equal equal difference bet them
 * Parameter:-String (second str)
 * Return type:-int     */

class compareToDemo {
	public static void main (String[] args) {
		String str1="Ashish" ;
		String str2="ashish" ;

		System.out.println(str1.compareTo(str2)) ;  // -32
	}
}

