import java.io.* ;
class myindexOfDemo {
	public static void main (String[] args)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		
		System.out.println("Enter string") ;
		String str=br.readLine() ;
		
		System.out.println("Enter char to be search") ;
		char ch=br.readLine().charAt(0) ;
		
		System.out.println("Enter starting index") ;
		int index=Integer.parseInt(br.readLine()) ;

		int ret=myindexOf(str,ch,index) ;
		System.out.println(ret) ;
		
	}

	static int myindexOf(String str,char ch,int index) {
		char arr[]=str.toCharArray() ;

		for (int i=index ; i<arr.length ; i++) {
			if (ch==arr[i])
				return i ;
		}
		return -1 ;
	}
}

		

