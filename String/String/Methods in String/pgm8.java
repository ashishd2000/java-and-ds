/* Method :-public int indexOf(char ch,fromIndex)
 * Parameter:-char,integer
 * Return type:-integer   */

class indexOfDemo {
	public static void main (String[] args) {
		String str1="Shashi" ;
	

		System.out.println(str1.indexOf('h',0)) ;  // 1
		System.out.println(str1.indexOf('h',1)) ;  // 1
		System.out.println(str1.indexOf('h',2)) ;  // 4
		System.out.println(str1.indexOf('h',5)) ;  // -1
	}
}

