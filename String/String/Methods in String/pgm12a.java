import java.io.* ;
class mysubstringDemo {
	public static void main (String[] args)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		
		System.out.println("Enter string") ;
		String str=br.readLine() ;
		
		System.out.println("Enter starting index") ;
		int startI=Integer.parseInt(br.readLine()) ;
		
		System.out.println("Enter ending index") ;
		int endI=Integer.parseInt(br.readLine()) ;

		String ret=myreplace(str,startI,endI) ;
		System.out.println(ret) ;
		
	}

	static String myreplace(String str,int startI,int endI) {
		char arr1[]=str.toCharArray() ;
		char arr2[]=new char[endI-startI] ;
		
		int i=0 ;
		for (int j=startI ; j<endI ; j++) {
			arr2[i]=arr1[j] ;
			i++ ;
		}
		String str1=new String(arr2) ;
		return str1 ;
	}
}

		

