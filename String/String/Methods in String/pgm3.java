/* Method 3:-public char charAt(int index)
 * Description:-It returns the characters located
 * at specified index within the given string
 * Parameter:-integer (index)
 * Return type:-character     */

class charAtDemo {
	public static void main (String[] args) {
		String str="Core2Web" ;

		System.out.println(str.charAt(4)) ;  // 2
		System.out.println(str.charAt(0)) ;  // c
		System.out.println(str.charAt(8)) ;  // StringindexOutofBoundsException
	}
}

