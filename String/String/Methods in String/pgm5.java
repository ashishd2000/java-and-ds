/* Method 5:-public int compareToIgnoreCase(String str2)
 * Description:-It compares the str1 and str2
 * (case insensitive),if both the strings are equal
 * it returns 0 otherwise returns the comparision
 * If both strings are equal returns 0
 * If strings are not equal equal difference bet them
 * Parameter:-String (second str)
 * Return type:-int     */

class compareToIgnoreToDemo {
	public static void main (String[] args) {
		String str1="SHASHI" ;
		String str2="shashi" ;

		System.out.println(str1.compareToIgnoreCase(str2)) ;  // -32
		
		String str3="shashi" ;
		String str4="shashikant" ;

		System.out.println(str3.compareToIgnoreCase(str4)) ;  // -4  returns the difference bet  length
	}
}

