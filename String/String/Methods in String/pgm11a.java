import java.io.* ;
class mysubstringDemo {
	public static void main (String[] args)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in)) ;
		
		System.out.println("Enter string") ;
		String str=br.readLine() ;
		
		System.out.println("Enter starting index") ;
		int index=Integer.parseInt(br.readLine()) ;

		String ret=myreplace(str,index) ;
		System.out.println(ret) ;
		
	}

	static String myreplace(String str,int index) {
		char arr1[]=str.toCharArray() ;
		char arr2[]=new char[arr1.length-index] ;
		
		int i=0 ;
		for (int j=index ; j<arr1.length ; j++) {
			arr2[i]=arr1[j] ;
			i++ ;
		}
		String str1=new String(arr2) ;
		return str1 ;
	}
}

		

