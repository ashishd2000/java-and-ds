/* Method 1:-Concat()
 * Description:-Concatinate to this string i.e.Another string
 * is concatinated with the first string.
 * Implements new array of character whose length is sum of
 * str1.length and str2.length
 * Parameter:-String
 * Return type:-String          */

class concatDemo {
	public static void main (String[] args) {
		String str1="Core2" ;
		String str2="Web" ;
		String str3=str1.concat(str2) ;

		System.out.println(str3) ;
	}
}

