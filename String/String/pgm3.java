class demo {
	public static void main (String[] args) {
		String str1="Kanha" ;  // String Constant Pool (Heap)
		String str2=str1 ; // String Constant Pool (Heap)
		String str3=new String(str2) ;  // Heap
						     
		System.out.println(System.identityHashCode(str1)) ;  // 1000
		System.out.println(System.identityHashCode(str2)) ;  // 1000
		System.out.println(System.identityHashCode(str3)) ;  // 3000   

	}
}
