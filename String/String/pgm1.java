class demo {
	public static void main (String[] args) {
		String str1="Core2Web" ;  // String Constant Pool (Heap)
		String str2=new String("Core2Web") ; // Heap
		String str3="Core2Web" ;  //String Constant pool (Heap)
		String str4=new String("Core2Web") ; // Heap
						     
		System.out.println(System.identityHashCode(str1)) ;  // 1000
		System.out.println(System.identityHashCode(str2)) ;  // 2000
		System.out.println(System.identityHashCode(str3)) ;  // 1000   
		System.out.println(System.identityHashCode(str4)) ;  // 3000

	}
}
