class demo {
	public static void main (String[] args) {
		String str1="Ashish" ;
		String str2=new String("Ashish") ;
		String str3="Ashish" ;
		String str4=new String("Ashish") ;

		/* Strings having same data have same hashCode */

		System.out.println(str1.hashCode()) ;
		System.out.println(str2.hashCode()) ;
		System.out.println(str3.hashCode()) ;
		System.out.println(str4.hashCode()) ;
	}
}
